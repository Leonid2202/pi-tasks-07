#pragma once
#include <iostream>
#include <string>
#include "BaseClasses.h"

using namespace std;

class Engineer : public Employee, public WorkTime, public Project
{
protected:
	int base;
	string projectName;
	int projectBudget;
	double involvement;
public:
	Engineer() : Employee(), projectName(""), projectBudget(0), involvement(0) {};
	Engineer(int _id, string _name, int _worktime, int _payment, int _base, string _projectName, int _projectBudget, double _involvement) :
		Employee(_id, _name, _worktime, _payment), base(_base), projectName(_projectName), projectBudget(_projectBudget), involvement(_involvement) {};

	int worktimePayment(int _worktime, int _base) override
	{
		return _worktime * _base;
	}

	int projectPayment(int _budget, double _involvement) override
	{
		return (int)(_budget * _involvement);
	}

	void paymentCalculation() override
	{
		payment = worktimePayment(worktime, base) + projectPayment(projectBudget, involvement);
	}
};

class Programmer : public Engineer
{
public:
	Programmer() : Engineer() {};
	Programmer(int _id, string _name, int _worktime, int _payment, int _base, string _projectName, int _projectBudget, double _involvement) :
		Engineer(_id, _name, _worktime, _payment, _base, _projectName, _projectBudget, _involvement) {};

	void showInfo(ostream& stream) override
	{
		stream << "Id: " << id << "\nName: " << name << "\nPosition: Programmer\nWorktime: " << worktime << "\nBase: " << base;
		stream << "\nProject Name: " << projectName << "\nProject Budget: " << projectBudget << "\nInvolvement: " << involvement;
		stream << "\nPayment: " << payment;
		stream << "\n----------------------\n";
	}
};

class Tester : public Engineer
{
public:
	Tester() : Engineer() {};
	Tester(int _id, string _name, int _worktime, int _payment, int _base, string _projectName, int _projectBudget, double _involvement) :
		Engineer(_id, _name, _worktime, _payment, _base, _projectName, _projectBudget, _involvement) {};

	void showInfo(ostream& stream) override
	{
		stream << "Id: " << id << "\nName: " << name << "\nPosition: Tester\nWorktime: " << worktime << "\nBase: " << base;
		stream << "\nProject Name: " << projectName << "\nProject Budget: " << projectBudget << "\nInvolvement: " << involvement;
		stream << "\nPayment: " << payment;
		stream << "\n----------------------\n";
	}
};

class TeamLeader : public Programmer, public Heading
{
protected:
	int feeForOne;
	int nSubordinates;
public:
	TeamLeader() : Programmer(), feeForOne(0), nSubordinates(0) {};
	TeamLeader(int _id, string _name, int _worktime, int _payment, int _base, string _projectName, int _projectBudget, double _involvement, int _feeForOne, int _nSubordinates) :
		Programmer(_id, _name, _worktime, _payment, _base, _projectName, _projectBudget, _involvement), feeForOne(_feeForOne), nSubordinates(_nSubordinates) {};

	int headingPayment(int _feeForOne, int _nSubordinates) override
	{
		return _feeForOne * _nSubordinates;
	}

	void paymentCalculation() override
	{
		payment = worktimePayment(worktime, base) + projectPayment(projectBudget, involvement) + headingPayment(feeForOne, nSubordinates);
	}

	void showInfo(ostream& stream) override
	{
		stream << "Id: " << id << "\nName: " << name << "\nPosition: Team Leader\nWorktime: " << worktime << "\nBase: " << base;
		stream << "\nProject Name: " << projectName << "\nProject Budget: " << projectBudget << "\nInvolvement: " << involvement;
		stream << "\nFee for each subordinate: " << feeForOne << "\nNumber of subordinates: " << nSubordinates;
		stream << "\nPayment: " << payment;
		stream << "\n----------------------\n";
	}
};