#pragma once
#include <iostream>
#include <string>
#include "BaseClasses.h"

using namespace std;

class Manager : public Employee, public Project
{
protected:
	string projectName;
	int projectBudget;
	double involvement;
public:
	Manager() : Employee(), projectName(""), projectBudget(0), involvement(0) {};
	Manager(int _id, string _name, int _worktime, int _payment, string _projectName, int _projectBudget, double _involvement) :
		Employee(_id, _name, _worktime, _payment), projectName(_projectName), projectBudget(_projectBudget), involvement(_involvement) {};

	int projectPayment(int _budget, double _involvement) override
	{
		return (int)(_budget * _involvement);
	}

	void paymentCalculation() override
	{
		payment = projectPayment(projectBudget, involvement);
	}

	void showInfo(ostream& stream) override
	{
		stream << "Id: " << id << "\nName: " << name << "\nPosition: Manager";
		stream << "\nProject Name: " << projectName << "\nProject Budget: " << projectBudget << "\nInvolvement: " << involvement;
		stream << "\nPayment: " << payment;
		stream << "\n----------------------\n";
	}
};

class ProjectManager : public Manager, public Heading
{
protected:
	int feeForOne;
	int nSubordinates;
public:
	ProjectManager() : Manager(), feeForOne(0), nSubordinates(0) {};
	ProjectManager(int _id, string _name, int _worktime, int _payment, string _projectName, int _projectBudget, double _involvement, int _feeForOne, int _nSubordinates) :
		Manager(_id, _name, _worktime, _payment, _projectName, _projectBudget, _involvement), feeForOne(_feeForOne), nSubordinates(_nSubordinates) {};

	int headingPayment(int _feeForOne, int _nSubordinates) override
	{
		return _feeForOne * _nSubordinates;
	}

	void paymentCalculation() override
	{
		payment = projectPayment(projectBudget, involvement) + headingPayment(feeForOne, nSubordinates);
	}
	void showInfo(ostream& stream) override
	{
		stream << "Id: " << id << "\nName: " << name << "\nPosition: Project Manager";
		stream << "\nProject Name: " << projectName << "\nProject Budget: " << projectBudget << "\nInvolvement: " << involvement;
		stream << "\nFee for each subordinate: " << feeForOne << "\nNumber of subordinates: " << nSubordinates;
		stream << "\nPayment: " << payment;
		stream << "\n----------------------\n";
	}
};

class SeniorManager : public ProjectManager
{
protected:
	int nProjects;
public:
	SeniorManager() : ProjectManager(), nProjects(0) {};
	SeniorManager(int _id, string _name, int _worktime, int _payment, string _projectName, int _projectBudget,
		double _involvement, int _feeForOne, int _nSubordinates, int _nProjects) :
		ProjectManager(_id, _name, _worktime, _payment, _projectName, _projectBudget, _involvement, _feeForOne, _nSubordinates), nProjects(_nProjects) {};


	int projectPayment(int _budget, double _involvement) override
	{
		return (int)(_budget * _involvement) * nProjects;
	}
	void showInfo(ostream& stream) override
	{
		stream << "Id: " << id << "\nName: " << name << "\nPosition: Senior Manager";
		stream << "\nProject Name: " << projectName << "\nProject Budget: " << projectBudget << "\nInvolvement: " << involvement << "\nNumber of projects: " << nProjects;
		stream << "\nFee for each subordinate: " << feeForOne << "\nNumber of subordinates: " << nSubordinates;
		stream << "\nPayment: " << payment;
		stream << "\n----------------------\n";
	}
};