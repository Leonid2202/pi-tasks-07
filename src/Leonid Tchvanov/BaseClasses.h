#pragma once
#include <string>
#include <iostream>

using namespace std;

class Employee
{
protected:
	int id;
	string name;
	int worktime;
	int payment;

public:
	Employee() : id(0), name(""), worktime(0), payment(0) {};
	Employee(int _id, string _name, int _worktime, int _payment) : id(_id), name(_name), worktime(_worktime), payment(_payment) {};
	void setID(int _id) { id = _id; }
	void setName(string _name) { name = _name; }
	void setWorkTime(int _worktime) { worktime = _worktime; }
	int getID() { return id; }
	string getName() { return name; }
	int getWorkTime() { return worktime; }

	virtual void showInfo(ostream& stream) = 0;
	virtual void paymentCalculation() = 0;
};

class WorkTime
{
	virtual int worktimePayment(int _worktime, int _base) = 0;
};

class Project
{
	virtual int projectPayment(int _budget, double _involvement) = 0;
};

class Heading
{
	virtual int headingPayment(int _feeForOne, int _nSubordinates) = 0;
};