#pragma once
#include <iostream>
#include <string>
#include "BaseClasses.h"

using namespace std;

class Personal : public Employee, public WorkTime
{
protected:
	int base;
public:
	Personal() : Employee(), base(0) {};
	Personal(int _id, string _name, int _worktime, int _payment, int _base) : Employee(_id, _name, _worktime, _payment), base(_base) {};

	int worktimePayment(int _worktime, int _base) override
	{
		return _worktime * _base;
	}
	void paymentCalculation() override
	{
		payment = worktimePayment(worktime, base);
	}
};

class Cleaner : public Personal
{
public:
	Cleaner() : Personal() {};
	Cleaner(int _id, string _name, int _worktime, int _payment, int _base) : Personal(_id, _name, _worktime, _payment, _base) {};

	void showInfo(ostream& stream) override
	{
		stream << "Id: " << id << "\nName: " << name <<  "\nPosition: Cleaner\nWorktime: " << worktime << "\nBase: " << base << "\nPayment: " << payment;
		stream << "\n----------------------\n";
	}
};

class Driver : public Personal
{
public:
	Driver() : Personal() {};
	Driver(int _id, string _name, int _worktime, int _payment, int _base) : Personal(_id, _name, _worktime, _payment, _base) {};

	void showInfo(ostream& stream) override
	{
		stream << "Id: " << id << "\nName: " << name << "\nPosition: Driver\nWorktime: " << worktime << "\nBase: " << base << "\nPayment: " << payment;
		stream << "\n----------------------\n";
	}
};