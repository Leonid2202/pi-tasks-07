#include <iostream>
#include <fstream>
#include <string>
#include "Personal.h"
#include "Manager.h"
#include "Engineer.h"
#include "Personal.h"

using namespace std;

int toInt(string str)
{
	return atoi(str.c_str());
}

double toDouble(string str)
{
	return atof(str.c_str());
}

int main()
{
	const int maxStaff = 40;
	Employee* staff[maxStaff];
	int curStaff = 0;
	ifstream inputFile("input data.txt");
	if (inputFile.is_open())
	{
		string curId, curName, curPosition, curWorktime, curBase, curProjectName, curProjectBudget,
			curInvolvement, curFeeForOne, curNSubardinates, curNProjects, tmp;
		while (!inputFile.eof())
		{
			getline(inputFile, curId);
			getline(inputFile, curName);
			getline(inputFile, curPosition);
			getline(inputFile, curWorktime);

			if (curPosition == "Cleaner")
			{
				getline(inputFile, curBase);

				staff[curStaff++] = new Cleaner(toInt(curId), curName, toInt(curWorktime), 0, toInt(curBase));
				continue;
			}
			if (curPosition == "Driver")
			{
				getline(inputFile, curBase);
				staff[curStaff++] = new Driver(toInt(curId), curName, toInt(curWorktime), 0, toInt(curBase));
				continue;
			}
			if (curPosition == "Programmer")
			{
				getline(inputFile, curBase);
				getline(inputFile, curProjectName);
				getline(inputFile, curProjectBudget);
				getline(inputFile, curInvolvement);
				staff[curStaff++] = new Programmer(toInt(curId), curName, toInt(curWorktime), 0, toInt(curBase),
					curProjectName, toInt(curProjectBudget), toDouble(curInvolvement));
				continue;
			}
			if (curPosition == "Tester")
			{
				getline(inputFile, curBase);
				getline(inputFile, curProjectName);
				getline(inputFile, curProjectBudget);
				getline(inputFile, curInvolvement);
				staff[curStaff++] = new Tester(toInt(curId), curName, toInt(curWorktime), 0, toInt(curBase),
					curProjectName, toInt(curProjectBudget), toDouble(curInvolvement));
				continue;
			}
			if (curPosition == "TeamLeader")
			{
				getline(inputFile, curBase);
				getline(inputFile, curProjectName);
				getline(inputFile, curProjectBudget);
				getline(inputFile, curInvolvement);
				getline(inputFile, curFeeForOne);
				getline(inputFile, curNSubardinates);
				staff[curStaff++] = new TeamLeader(toInt(curId), curName, toInt(curWorktime), 0, toInt(curBase),
					curProjectName, toInt(curProjectBudget), toDouble(curInvolvement), toInt(curFeeForOne), toInt(curNSubardinates));
				continue;
			}
			if (curPosition == "Manager")
			{
				getline(inputFile, curProjectName);
				getline(inputFile, curProjectBudget);
				getline(inputFile, curInvolvement);
				staff[curStaff++] = new Manager(toInt(curId), curName, toInt(curWorktime), 0,
					curProjectName, toInt(curProjectBudget), toDouble(curInvolvement));
				continue;
			}
			if (curPosition == "ProjectManager")
			{
				getline(inputFile, curProjectName);
				getline(inputFile, curProjectBudget);
				getline(inputFile, curInvolvement);
				getline(inputFile, curFeeForOne);
				getline(inputFile, curNSubardinates);
				staff[curStaff++] = new ProjectManager(toInt(curId), curName, toInt(curWorktime), 0,
					curProjectName, toInt(curProjectBudget), toDouble(curInvolvement), toInt(curFeeForOne), toInt(curNSubardinates));
				continue;
			}

			if (curPosition == "SeniorManager")
			{
				getline(inputFile, curProjectName);
				getline(inputFile, curProjectBudget);
				getline(inputFile, curInvolvement);
				getline(inputFile, curFeeForOne);
				getline(inputFile, curNSubardinates);
				getline(inputFile, curNProjects);
				staff[curStaff++] = new SeniorManager(toInt(curId), curName, toInt(curWorktime), 0,
					curProjectName, toInt(curProjectBudget), toDouble(curInvolvement), toInt(curFeeForOne), toInt(curNSubardinates), toInt(curNProjects));
				continue;
			}
		}
	}
	inputFile.close();
	ofstream outputFile("output data.txt");
	for (int i = 0; i < curStaff; i++)
	{
		staff[i]->paymentCalculation();
		staff[i]->showInfo(outputFile);
	}
}


